<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Calculs\Conversions;

class ConversionsController extends Controller
{
    public function getViewTemperature()
    {
        //Initialisation de variables
        $temperature = 0;
        $result = "";

		return view('temperature', [
            'temperatureHTML' => $temperature,
            'uniteInitiale' => "°C",
            'unite' => "°C",
            'resultHTML' => $result,
        ]);
    }

    public function calcTemperature(){
       //On recupere les elements via l'attribut name dans le formulaire
        $temperature = request(key:'temperatureVal');
        $uniteInitiale = request(key:'uniteInitiale');
        $unite = request(key:'unite');

        //methode à utiliser
        $res = Conversions::convertTemp($temperature,$uniteInitiale,$unite);
        //Retour utilisateur
        $result = $temperature." ".$uniteInitiale." équivaut à ".$res." ".$unite.".";

        //Renvoyer la vue temperature.blade.php
        return view('temperature',
        [
            'temperatureHTML' => $temperature,
            'uniteInitiale' => $uniteInitiale,
            'unite' => $unite,
            'resultHTML' => $result
        ]);
    }
}
