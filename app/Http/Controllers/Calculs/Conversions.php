<?php

namespace App\Http\Controllers\Calculs;

class Conversions
{
    public static function convertTemp($temperature, $uniteInitiale, $unite){
       if($uniteInitiale == $unite) {
           return $temperature;
       }
       elseif ($uniteInitiale == "°C" & $unite == "°F") {
           return $temperature * 9 / 5 + 32;
       }
       elseif ($uniteInitiale == "°F" & $unite == "°C") {
           return ($temperature - 32) * 5 / 9;
       }
       return 0;
    }

}
