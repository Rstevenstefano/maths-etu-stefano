<?php

namespace Tests\Unit;

use App\Http\Controllers\Calculs\Conversions;
use PHPUnit\Framework\TestCase;

class ConversionsTest extends TestCase
{
    /**
     * Test conversion temp c to f
     *
     * @return void
     */
    public function testConvertTempCtoF()
    {
        $temperature = 5;
        $uniteInitiale = "°C";
        $unite = "°F";

        //Resultat à verifier avec expected
        $resultat = Conversions::convertTemp($temperature,$uniteInitiale,$unite);
        $expected = 41;
        $this->assertSame($expected,$resultat);
    }

    /**
     *Test conversion temp f to c
     * @return void
     */
    public function testConvertTempFtoC(){
        $temperature = 41;
        $uniteInitiale = "°F";
        $unite = "°C";

        //Resultat à verifier avec expected
        $resultat = Conversions::convertTemp($temperature,$uniteInitiale,$unite);
        $expected = 5;
        $this->assertSame($expected,$resultat);
    }

    /**
     *Test conversion temp c to c or f to f
     * @return void
     */
    public function testConvertTempSameUnit(){
        $temperature = 5;
        $uniteInitiale = "°C";
        $unite = "°C";

        //Resultat à verifier avec expected
        $resultat = Conversions::convertTemp($temperature,$uniteInitiale,$unite);
        $expected = 5;
        $this->assertSame($expected,$resultat);
    }

}
