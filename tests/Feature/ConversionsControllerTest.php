<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ConversionsControllerTest extends TestCase
{
    /**
     * Get test of get view temperature
     *
     * @return void
     */
    public function testGetViewTemperature()
    {
        //Variables
        $temperature = 0;
        $uniteInitiale = "°C";
        $unite = "°C";

        //Appel de vue
        $reponse = $this->get('/temperature',[
            'temperatureHTML' => $temperature, 'uniteInitiale'=>$uniteInitiale,
            'unite' => $unite, 'resultHTML'=>''
        ]);

        //Recup contenu html
        $content = $reponse->content();
        //Verifie si le contenu contient bien la balise h2 avec le titre
        $this->assertTrue(str_contains($content,'<h2 id="title-page">Température</h2>'));

    }
    /**
     * POST test of post view temperature
     *
     * @return void
     */
    public function testPostViewTemperature()
    {
        //Variables
        $temperature = 5;
        $uniteInitiale = "°F";
        $unite = "°C";

        //Appel de vue
        $reponse = $this->call('POST',[
            'temperatureVal' => $temperature, 'uniteInitiale'=>$uniteInitiale,
            'unite' => $unite, 'resultHTML'=>''
        ]);
        //Resultat attendu
        $expected ='5°F équivaut à -15°C.';
        //resultat
        $result = $reponse->viewData('resultHTML');

        //On verifie tout le contenu du view data
        $this->assertSame($expected,$result);
        $this->assertSame($temperature, $reponse->viewData('temperatureHTML'));
        $this->assertSame($uniteInitiale, $reponse->viewData('uniteInitiale'));
        $this->assertSame($unite, $reponse->viewData('unite'));
    }


}
